package and.pos.pragmatic.pragmatic_pos.Model;

/**
 * Created by kundan on 6/23/2015.
 */
public class Globals {


    private static Globals instance = new Globals();

    // Getter-Setters
    public static Globals getInstance() {
        return instance;
    }

    public static void setInstance(Globals instance) {
        Globals.instance = instance;
    }

    private String dataPrint;

    private Globals() {

    }


    public String getValue() {
        return dataPrint;
    }


    public void setValue(String notification_index) {
        this.dataPrint = notification_index;
    }



}