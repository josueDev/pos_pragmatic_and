package and.pos.pragmatic.pragmatic_pos.Model;

import java.util.ArrayList;

public class ProductosRepositorio {

    public ArrayList<Productos> productos;

    private ProductosRepositorio() {
        productos = new ArrayList<Productos>();
    }

    private static ProductosRepositorio instance;

    public static ProductosRepositorio getInstance() {
        if (instance == null) instance = new ProductosRepositorio();
        return instance;
    }
}
