package and.pos.pragmatic.pragmatic_pos;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import and.pos.pragmatic.pragmatic_pos.lib.Constantes;
import and.pos.pragmatic.pragmatic_pos.lib.VolleySingleton;

public class RegistryActivity extends Activity {

    TextView campoId;
    TextView campoContraseña;
    FrameLayout mainFrame;
    ProgressDialog mWaitingDialog;
    Button botonRegistrar,botonTicket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registry);
        RegistryActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new
                    StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        mainFrame = (FrameLayout) findViewById(R.id.registry_layout);

        campoId = (TextView) findViewById(R.id.idterminal);
        campoContraseña = (TextView) findViewById(R.id.passwordterminal);
        botonRegistrar = (Button) findViewById(R.id.buttonRegistrar);
        botonTicket = (Button) findViewById(R.id.buttonTicket);

        //Verifica si ya esta registrada la terminal
        SharedPreferences preferences = RegistryActivity.this.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        String isRegistry = preferences.getString("isRegister", "false");

        if (isRegistry.equals("true") )
        {
            Intent i = new Intent(RegistryActivity.this, DecoderActivity.class);
            //i.putExtras(sendBundle);
            startActivity(i);
        }
        else
        {

        }

        botonRegistrar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                SharedPreferences preferences = RegistryActivity.this.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("isRegister", "true");
                editor.commit();

                Intent i = new Intent(RegistryActivity.this, DecoderActivity.class);
                //i.putExtras(sendBundle);
                startActivity(i);
            }
        });

        botonTicket.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // your handler code here
            }
        });

        //qrCodeReaderView.setOnQRCodeReadListener(this);

    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    public void getTicket() {

        mWaitingDialog = ProgressDialog.show(RegistryActivity.this, "", "Cargando...", true);
        mWaitingDialog.setIndeterminate(false);

        VolleySingleton.
                getInstance(getApplication()).
                addToRequestQueue(
                        new JsonObjectRequest(
                                Request.Method.POST,
                                Constantes.POS,
                                new JSONObject(),
                                new Response.Listener() {

                                    @Override
                                    public void onResponse(Object response) {

                                        // Procesar la respuesta Json
                                        //procesarRespuesta(response);
                                        Log.d("response", response.toString());

                                        //qrCodeReaderView.startCamera();

                                        new Thread(new Runnable() {
                                            public void run() {
                                                runOnUiThread(new Runnable() {
                                                    public void run() {
                                                        mWaitingDialog.dismiss();
                                                    }
                                                });
                                            }
                                        }).start();

                                    }


                                },

                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        /*

                                        new Thread(new Runnable() {
                                            public void run() {
                                                runOnUiThread(new Runnable() {
                                                    public void run() {
                                                        mWaitingDialog.dismiss();
                                                    }
                                                });
                                            }
                                        }).start();

                                        Log.d("", "Response: " + error.getMessage());
                                        if (error.getMessage() == null)
                                        {
                                            showAlertDialog("Ha ocurrido un error en el servidor", "");
                                            //qrCodeReaderView.startCamera();
                                        }
                                        else {
                                            String str = error.getMessage().toString();
                                            final String[] splited = str.split("\\s+");
                                            Log.d("", "RESPONSE: " + splited[2]);

                                            new Thread(new Runnable() {
                                                public void run() {

                                                    runOnUiThread(new Runnable() {
                                                        public void run() {
                                                            qrCodeReaderView.setVisibility(View.INVISIBLE);
                                                            contenedorMensaje.setVisibility(View.VISIBLE);

                                                            switch (splited[2]) {
                                                                case "Falta":
                                                                    textoMensaje.setText(splited[2]);
                                                                    contenedorMensaje.setBackgroundColor(Color.parseColor("#AA0000"));
                                                                    break;
                                                                case "A":
                                                                    textoMensaje.setText("Asistencia");
                                                                    contenedorMensaje.setBackgroundColor(Color.parseColor("#00AA00"));
                                                                    break;
                                                                case "Retardo":
                                                                    textoMensaje.setText(splited[2]);
                                                                    contenedorMensaje.setBackgroundColor(Color.parseColor("#7F00FF"));
                                                                    break;
                                                                default:
                                                                    textoMensaje.setText("Ha ocurrido un error");
                                                                    contenedorMensaje.setBackgroundColor(Color.parseColor("#AA0000"));
                                                                    break;
                                                            }
                                                        }
                                                    });


                                                    try {
                                                        Thread.sleep(2000);
                                                    } catch (InterruptedException e) {
                                                        e.printStackTrace();
                                                    }

                                                    runOnUiThread(new Runnable() {
                                                        public void run() {
                                                            contenedorMensaje.setVisibility(View.INVISIBLE);
                                                            qrCodeReaderView.setVisibility(View.VISIBLE);
                                                            qrCodeReaderView.startCamera();
                                                        }
                                                    });

                                                }
                                            }).start();
                                        }
*/
                                    }
                                }

                        )
                );
    }




    public void registerTerminal() {

        mWaitingDialog = ProgressDialog.show(RegistryActivity.this, "", "Cargando...", true);
        mWaitingDialog.setIndeterminate(false);

        VolleySingleton.
                getInstance(getApplication()).
                addToRequestQueue(
                        new JsonObjectRequest(
                                Request.Method.POST,
                                Constantes.POS,
                                new JSONObject(),
                                new Response.Listener() {

                                    @Override
                                    public void onResponse(Object response) {

                                        // Procesar la respuesta Json
                                        //procesarRespuesta(response);
                                        Log.d("response", response.toString());

                                        //qrCodeReaderView.startCamera();

                                        new Thread(new Runnable() {
                                            public void run() {
                                                runOnUiThread(new Runnable() {
                                                    public void run() {
                                                        mWaitingDialog.dismiss();
                                                    }
                                                });
                                            }
                                        }).start();

                                    }


                                },

                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        /*

                                        new Thread(new Runnable() {
                                            public void run() {
                                                runOnUiThread(new Runnable() {
                                                    public void run() {
                                                        mWaitingDialog.dismiss();
                                                    }
                                                });
                                            }
                                        }).start();

                                        Log.d("", "Response: " + error.getMessage());
                                        if (error.getMessage() == null)
                                        {
                                            showAlertDialog("Ha ocurrido un error en el servidor", "");
                                            //qrCodeReaderView.startCamera();
                                        }
                                        else {
                                            String str = error.getMessage().toString();
                                            final String[] splited = str.split("\\s+");
                                            Log.d("", "RESPONSE: " + splited[2]);

                                            new Thread(new Runnable() {
                                                public void run() {

                                                    runOnUiThread(new Runnable() {
                                                        public void run() {
                                                            qrCodeReaderView.setVisibility(View.INVISIBLE);
                                                            contenedorMensaje.setVisibility(View.VISIBLE);

                                                            switch (splited[2]) {
                                                                case "Falta":
                                                                    textoMensaje.setText(splited[2]);
                                                                    contenedorMensaje.setBackgroundColor(Color.parseColor("#AA0000"));
                                                                    break;
                                                                case "A":
                                                                    textoMensaje.setText("Asistencia");
                                                                    contenedorMensaje.setBackgroundColor(Color.parseColor("#00AA00"));
                                                                    break;
                                                                case "Retardo":
                                                                    textoMensaje.setText(splited[2]);
                                                                    contenedorMensaje.setBackgroundColor(Color.parseColor("#7F00FF"));
                                                                    break;
                                                                default:
                                                                    textoMensaje.setText("Ha ocurrido un error");
                                                                    contenedorMensaje.setBackgroundColor(Color.parseColor("#AA0000"));
                                                                    break;
                                                            }
                                                        }
                                                    });


                                                    try {
                                                        Thread.sleep(2000);
                                                    } catch (InterruptedException e) {
                                                        e.printStackTrace();
                                                    }

                                                    runOnUiThread(new Runnable() {
                                                        public void run() {
                                                            contenedorMensaje.setVisibility(View.INVISIBLE);
                                                            qrCodeReaderView.setVisibility(View.VISIBLE);
                                                            qrCodeReaderView.startCamera();
                                                        }
                                                    });

                                                }
                                            }).start();
                                        }
*/
                                    }
                                }

                        )
                );
    }



    /*
    private void procesarRespuesta(JSONObject response) {
        try {
            // Obtener atributo "estado"
            String estado = response.getString("estado");

            switch (estado) {
                case "1": // EXITO
                    // Obtener array "metas" Json
                    JSONArray mensaje = response.getJSONArray("metas");
                    // Parsear con Gson
                    Meta[] metas = gson.fromJson(mensaje.toString(), Meta[].class);
                    // Inicializar adaptador
                    adapter = new MetaAdapter(Arrays.asList(metas), getActivity());
                    // Setear adaptador a la lista
                    lista.setAdapter(adapter);
                    break;
                case "2": // FALLIDO
                    String mensaje2 = response.getString("mensaje");
                    Toast.makeText(
                            getActivity(),
                            mensaje2,
                            Toast.LENGTH_LONG).show();
                    break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    */

    private void showAlertDialog(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(true);
        builder.setPositiveButton(
                "Deacuerdo",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
/*
        builder.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
*/

        AlertDialog alert = builder.create();
        alert.show();
    }

}