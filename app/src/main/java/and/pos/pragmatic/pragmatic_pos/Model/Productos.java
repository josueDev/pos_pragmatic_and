package and.pos.pragmatic.pragmatic_pos.Model;

public class Productos {
    private int cantidad;
    private int id;
    private int idProductoExterno;
    private String nombre;
    private Double precio;
    private Double total;

    public Productos(int id, String nombre, Double precio, int cantidad, Double total, int idProductoExterno) {
        this.id = id;
        this.nombre = nombre;
        this.precio = precio;
        this.cantidad = cantidad;
        this.total = total;
        this.idProductoExterno = idProductoExterno;
    }

    public int getCantidad() {
        return this.cantidad;
    }

    public int getID() {
        return this.id;
    }

    public int getId() {
        return this.id;
    }

    public String getNombre() {
        return this.nombre;
    }

    public Double getPrecio() {
        return this.precio;
    }

    public Double getTotal() {
        return this.total;
    }

    public int getidProductoExterno() {
        return this.idProductoExterno;
    }

    public void setCantidad(int n) {
        this.cantidad = n;
    }

    public void setId(int n) {
        this.id = n;
    }

    public void setIdProductoExterno(int n) {
        this.idProductoExterno = n;
    }

    public void setNombre(String string2) {
        this.nombre = string2;
    }

    public void setPrecio(Double d) {
        this.precio = d;
    }

    public void setTotal(Double d) {
        this.total = d;
    }
}