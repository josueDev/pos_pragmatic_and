package and.pos.pragmatic.pragmatic_pos;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PointF;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.dlazaro66.qrcodereaderview.QRCodeReaderView.OnQRCodeReadListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import and.pos.pragmatic.pragmatic_pos.lib.Constantes;
import and.pos.pragmatic.pragmatic_pos.lib.VolleySingleton;

import com.davidmiguel.numberkeyboard.NumberKeyboard;
import com.davidmiguel.numberkeyboard.NumberKeyboardListener;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import static android.util.TypedValue.*;
import static java.lang.Thread.sleep;
import java.text.NumberFormat;
import java.util.logging.Handler;

public class DecoderActivity extends Activity implements OnQRCodeReadListener, ActivityCompat.OnRequestPermissionsResultCallback, NumberKeyboardListener {

    //private TextView resultTextView;
    QRCodeReaderView qrCodeReaderView;
    LinearLayout contenedorMensaje;

    private static final int MY_PERMISSION_REQUEST_CAMERA = 0;

    TextView textoMensaje;
    TextView textoMensaje1;
    TextView textoMensaje2;
    TextView textoMensaje3;
    FrameLayout mainFrame;
    LinearLayout barTopButtons;
    FloatingActionButton closeButton;

    //public static ProgressDialog mWaitingDialog;
    boolean modeEnterPrice = false;
    String amountText = "";
    Double amount = 0.0;
    TextView amountEditText;

    Button buttonAcceptAmount,buttonNuevaCompra;
    FloatingActionButton switchButton;
    ImageView imageQR;
    NumberKeyboard numberKeyboard;

    boolean activarTecladoMonto = true;
    boolean modoGeneradorQR = false;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_decoder);
        DecoderActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        mainFrame = (FrameLayout) findViewById(R.id.main_layout);
        contenedorMensaje = (LinearLayout) findViewById(R.id.contenedormensaje);
        textoMensaje = (TextView) findViewById(R.id.textomensaje);
        textoMensaje1 = (TextView) findViewById(R.id.textView1);
        textoMensaje2 = (TextView) findViewById(R.id.textView2);
        textoMensaje3 = (TextView) findViewById(R.id.textView3);
        closeButton = (FloatingActionButton) findViewById(R.id.closeButton);
        barTopButtons = (LinearLayout) findViewById(R.id.barTopButtons);
        amountEditText = (TextView) findViewById(R.id.amount);
        buttonAcceptAmount = (Button) findViewById(R.id.buttonAcceptAmount);
        imageQR = (ImageView) findViewById(R.id.imageQR);
        switchButton = (FloatingActionButton) findViewById(R.id.switchButton);
        numberKeyboard =  findViewById(R.id.numberKeyboard);
        buttonNuevaCompra = findViewById(R.id.buttonNuevaCompra);

        numberKeyboard.setListener(this);


        textoMensaje1.setText("Por favor muestre su \ncódigo QR al Scanner");
        textoMensaje2.setText("Salón 1A");



        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            initQRCodeReaderView();
        } else {
            requestCameraPermission();
        }
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {

            @Override
            public void run() {

                DecoderActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy HH:mm:ss");
                        String currentDateandTime = sdf.format(new Date());
                        //Date currentTime = Calendar.getInstance().getTime();
                        textoMensaje3.setText(currentDateandTime.toString());
                    }
                });


            }
        },0,1000);//Update text every second

        closeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                SharedPreferences preferences = DecoderActivity.this.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("isRegister", "false");
                editor.commit();

                finish();

            }
        });

        switchButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (modoGeneradorQR == false)
                {
                    modoGeneradorQR = true;
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    imageQR.setVisibility(View.VISIBLE);
                                    ocultarElementosScanner();
                                }
                            });
                }
                else
                {
                    modoGeneradorQR = false;
                    runOnUiThread(new Runnable() {
                        public void run() {
                            imageQR.setVisibility(View.INVISIBLE);
                            mostrarElementosScanner();
                        }
                    });

                }
            }
        });

        buttonAcceptAmount.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (amountText.equals("0.0"))
                {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            contenedorMensaje.setVisibility(View.INVISIBLE);
                            qrCodeReaderView.setVisibility(View.VISIBLE);
                            qrCodeReaderView.startCamera();
                        }
                    });

                    Timer timer2 = new Timer();
                    timer2.schedule(new TimerTask() {
                        @Override
                        public void run() {

                            new Thread(new Runnable() {
                                public void run() {

                                    runOnUiThread(new Runnable() {
                                        public void run() {

                                            qrCodeReaderView.setVisibility(View.INVISIBLE);
                                            amountEditText.setText("$ 0.0");
                                            amount = 0.0;
                                            amountText = "";

                                        }
                                    });
                                }
                            }).start();



                        }
                    }, 5000);

                }
                else
                {

                }

            }
        });

        buttonNuevaCompra.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                runOnUiThread(new Runnable() {
                    public void run() {

                       // contenedorMensaje.setVisibility(View.INVISIBLE);
                       // qrCodeReaderView.setVisibility(View.VISIBLE);
                       // qrCodeReaderView.startCamera();

                        qrCodeReaderView.setVisibility(View.INVISIBLE);
                        amountEditText.setText("$ 0.0");
                        amount = 0.0;
                        amountText = "";
                    }
                });
            }
        });

        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new
                    StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        new Thread(new Runnable() {
            public void run() {

                runOnUiThread(new Runnable() {
                    public void run() {

                        if (activarTecladoMonto)
                        {
                            contenedorMensaje.setVisibility(View.INVISIBLE);
                        }

                        imageQR.setVisibility(View.INVISIBLE);
                        //qrCodeReaderView.setVisibility(View.INVISIBLE);
                        //numberKeyboard.setVisibility(View.INVISIBLE);
                        //numberKeyboard.setVisibility(View.VISIBLE);
                        //barTopButtons.setVisibility(View.GONE); // GONE REACOMODA LOS LAYOUTS
                        barTopButtons.setVisibility(View.VISIBLE);
                    }
                });
            }
        }).start();

        int dp = 1;
        final float scale = getResources().getDisplayMetrics().density;
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        final int i = (int) (dp * (width/13) + 0.0f);

        numberKeyboard.setKeyHeight(i);
        numberKeyboard.setKeyWidth(i);
        numberKeyboard.setMaxHeight(width/2);
        numberKeyboard.setMaxWidth(width/2);
        System.out.println(width);

        QRCodeWriter writer = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = writer.encode("cog erg gggerg err greg regre g cntenido", BarcodeFormat.QR_CODE, 1024, 1024);
            int width2 = bitMatrix.getWidth();
            int height2 = bitMatrix.getHeight();
            Bitmap bmp = Bitmap.createBitmap(width2, height2, Bitmap.Config.RGB_565);
            for (int x = 0; x < width2; x++) {
                for (int y = 0; y < height2; y++) {
                    bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.rgb(250,250,250));
                }
            }
            imageQR.setImageBitmap(bmp);
        } catch (WriterException e) {
            e.printStackTrace();
        }

        amountEditText.setText("$ 0.0");


    }


    @Override
    public void onQRCodeRead(String text, PointF[] points) {
        //resultTextView.setText(text);
        //Log.d("",text);
        if (text == "No QR Code found")
        {

        }
        else
        {
            Log.d("textQR",text);
            qrCodeReaderView.stopCamera();

            //final String[] splited = text.split("--");
            final String[] splited = text.split(":");
            Log.d("IDUSER",splited[1]);
            try {
                cargarAdaptador(splited[1]);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (MY_PERMISSION_REQUEST_CAMERA == 1)
        {
            qrCodeReaderView.startCamera();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (MY_PERMISSION_REQUEST_CAMERA == 1)
        {
            qrCodeReaderView.stopCamera();
        }
    }

    private void requestCameraPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
            Snackbar.make(mainFrame, "Camera access is required to display the camera preview.",
                    Snackbar.LENGTH_INDEFINITE).setAction("OK", new View.OnClickListener() {
                @Override public void onClick(View view) {
                    ActivityCompat.requestPermissions(DecoderActivity.this, new String[] {
                            Manifest.permission.CAMERA
                    }, MY_PERMISSION_REQUEST_CAMERA);
                }
            }).show();
        } else {
            Snackbar.make(mainFrame, "Permission is not available. Requesting camera permission.",
                    Snackbar.LENGTH_SHORT).show();
            ActivityCompat.requestPermissions(this, new String[] {
                    Manifest.permission.CAMERA
            }, MY_PERMISSION_REQUEST_CAMERA);
        }
    }

    @Override public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                                     @NonNull int[] grantResults) {
        if (requestCode != MY_PERMISSION_REQUEST_CAMERA) {
            return;
        }

        if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            initQRCodeReaderView();
            Snackbar.make(mainFrame, "Camera permission was granted.", Snackbar.LENGTH_SHORT).show();
            recreate();

        } else {
            Snackbar.make(mainFrame, "Camera permission request was denied.", Snackbar.LENGTH_SHORT)
                    .show();
        }
    }

    private void initQRCodeReaderView() {

        new Thread(new Runnable() {
            public void run() {

                runOnUiThread(new Runnable() {
                    public void run() {

                        qrCodeReaderView = (QRCodeReaderView) findViewById(R.id.qrdecoderview);
                        qrCodeReaderView.setAutofocusInterval(2000L);
                        qrCodeReaderView.setOnQRCodeReadListener(DecoderActivity.this);
                        qrCodeReaderView.setFrontCamera();
                        qrCodeReaderView.startCamera();

                        if (activarTecladoMonto)
                        {
                            qrCodeReaderView.setVisibility(View.INVISIBLE);
                        }



                    }
                });

            }
        }).start();


        //qrCodeReaderView.startCamera();
    }


    public void cargarAdaptador(String idUser) throws JSONException {

        new Thread(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {


                    }
                });
            }
        }).start();

        //mWaitingDialog = ProgressDialog.show(DecoderActivity.this, "", "Cargando...", true);
        //mWaitingDialog.setIndeterminate(false);


        JSONObject emptyJsonObject = new JSONObject("{}");
        try {

            emptyJsonObject.put("046", "027");
            emptyJsonObject.put("105", "15");

        } catch (JSONException e) {
            e.printStackTrace();
        }


        Log.i("URL ",Constantes.POS+idUser);
        VolleySingleton.
                getInstance(getApplication()).
                addToRequestQueue(
                        new JsonObjectRequest(
                                Request.Method.POST,
                                Constantes.POS+idUser,
                                emptyJsonObject,
                                new Response.Listener() {

                                    @Override
                                    public void onResponse(Object response) {

                                        // Procesar la respuesta Json
                                        //procesarRespuesta(response);
                                        Log.d("response", response.toString());

                                        qrCodeReaderView.startCamera();

                                        new Thread(new Runnable() {
                                            public void run() {
                                                runOnUiThread(new Runnable() {
                                                    public void run() {
                                                        try {
                                                            sleep(500);
                                                        } catch (InterruptedException e) {
                                                            e.printStackTrace();
                                                        }
                                                        //mWaitingDialog.dismiss();
                                                    }
                                                });
                                            }
                                        }).start();

                                    }

                                },

                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {

                                        //mWaitingDialog.dismiss();

                                        Log.d("", "Response: " + error.getMessage());
                                        if (error.getMessage() == null)
                                        {
                                            //showAlertDialog("Ha ocurrido un error en el servidor", "");
                                            Toast toast1 = Toast.makeText(getApplicationContext(), "Ha ocurrido un error en el servidor", Toast.LENGTH_SHORT);
                                            toast1.show();
                                            qrCodeReaderView.startCamera();
                                        }
                                        else {
                                            String str = error.getMessage().toString();
                                            Log.i("", "RESPONSEALL: " + str);
                                            final String[] splited = str.split("\\s+");
                                            Log.d("", "RESPONSE: " + splited[2]);

                                            new Thread(new Runnable() {
                                                public void run() {

                                                    runOnUiThread(new Runnable() {
                                                        public void run() {

                                                            qrCodeReaderView.setVisibility(View.INVISIBLE);
                                                            contenedorMensaje.setVisibility(View.VISIBLE);

                                                            switch (splited[2]) {
                                                                case "Falta":
                                                                    textoMensaje.setText(splited[2]);
                                                                    contenedorMensaje.setBackgroundColor(Color.parseColor("#AA0000"));
                                                                    break;
                                                                case "A":
                                                                    textoMensaje.setText("Asistencia");
                                                                    contenedorMensaje.setBackgroundColor(Color.parseColor("#00AA00"));
                                                                    break;
                                                                case "Retardo":
                                                                    textoMensaje.setText(splited[2]);
                                                                    contenedorMensaje.setBackgroundColor(Color.parseColor("#7F00FF"));
                                                                    break;
                                                                default:
                                                                    textoMensaje.setText("Ha ocurrido un error");
                                                                    contenedorMensaje.setBackgroundColor(Color.parseColor("#AA0000"));
                                                                    break;
                                                            }
                                                        }
                                                    });


                                                    try {
                                                        Thread.sleep(1500);
                                                    } catch (InterruptedException e) {
                                                        e.printStackTrace();
                                                    }

                                                    runOnUiThread(new Runnable() {
                                                        public void run() {

                                                            contenedorMensaje.setVisibility(View.INVISIBLE);
                                                            qrCodeReaderView.setVisibility(View.VISIBLE);
                                                            qrCodeReaderView.startCamera();
                                                        }
                                                    });

                                                }
                                            }).start();
                                        }

                                    }
                                }

                        )
                );
    }



    /*
    private void procesarRespuesta(JSONObject response) {
        try {
            // Obtener atributo "estado"
            String estado = response.getString("estado");

            switch (estado) {
                case "1": // EXITO
                    // Obtener array "metas" Json
                    JSONArray mensaje = response.getJSONArray("metas");
                    // Parsear con Gson
                    Meta[] metas = gson.fromJson(mensaje.toString(), Meta[].class);
                    // Inicializar adaptador
                    adapter = new MetaAdapter(Arrays.asList(metas), getActivity());
                    // Setear adaptador a la lista
                    lista.setAdapter(adapter);
                    break;
                case "2": // FALLIDO
                    String mensaje2 = response.getString("mensaje");
                    Toast.makeText(
                            getActivity(),
                            mensaje2,
                            Toast.LENGTH_LONG).show();
                    break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    */

    private void showAlertDialog(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(true);
        builder.setPositiveButton(
                "Deacuerdo",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

/*
        builder.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
*/

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onBackPressed() {
        // Simply Do noting!
    }


    @Override
    public void onLeftAuxButtonClicked() {
        if (!hasComma(amountText)) {
             if (amountText.isEmpty())
            {
                amountText = "0.";
            } else
            {
                amountText = amountText + ".";

            }
            showAmount(amountText);

        }

    }

    @Override
    public void onNumberClicked(int number) {

        if (amountText.isEmpty() && number == 0) {

        }
        updateAmount(amountText + number);

    }

    @Override
    public void onRightAuxButtonClicked() { // Delete button

        if (amountText.isEmpty()) {

        }
        String newAmountText = "";
        if (amountText.length() <= 1) {
            newAmountText = "";
        } else {
            newAmountText = amountText.substring(0, amountText.length() - 1);
            char[] sa = newAmountText.toCharArray();
            if (sa[newAmountText.length() - 1] == ',') {
                newAmountText = newAmountText.substring(0, newAmountText.length() - 1);
            }
            if ("0" == newAmountText) {
                newAmountText = "";
            }
        }
        updateAmount(newAmountText);

    }


    public void showAmount(String amount) {
        amountEditText.setText("$");
        if (amount.isEmpty())
        {
            amountEditText.setText("$ 0.0");
        } else
        {

            //amountEditText.setText(addThousandSeparator(amount));
            amountEditText.setText("$" + " " + (amount));
            //amountEditText.setText(amountText);
        }
    }


    public boolean hasComma(String text)  {
        char[] sa = text.toCharArray();
        for(int i=0; i<=text.length() - 1; i++) {
            if (sa[i] == '.') {
                return true;
            }
        }
        return false;
    }

    public void updateAmount( String newAmountText ) {

        Double newAmount = 0.0;
         if (newAmountText.isEmpty())
        {
            newAmount =  0.0;
        }
        else
        {
            //newAmount = java.lang.Double.parseDouble(newAmountText.replace(",".toRegex(), "."));
            newAmount = java.lang.Double.parseDouble(newAmountText);
        }


            amountText = newAmountText;
            amount = newAmount;
            showAmount(amountText);



    }

    private void ocultarElementosScanner()
    {
        buttonAcceptAmount.setVisibility(View.INVISIBLE);
        numberKeyboard.setVisibility(View.INVISIBLE);
        amountEditText.setVisibility(View.INVISIBLE);
    }

    private void mostrarElementosScanner()
    {

        buttonAcceptAmount.setVisibility(View.VISIBLE);
        numberKeyboard.setVisibility(View.VISIBLE);
        amountEditText.setVisibility(View.VISIBLE);
    }

    public String addThousandSeparator( String amount ) {

        String integer = "";
        String decimal = "";
        if (amount.indexOf(',') >= 0) {
            integer = amount.substring(0, amount.indexOf(','));
            decimal = amount.substring(amount.indexOf(','), amount.length());
        } else {
            integer = amount;
            decimal = "";
        }
        if (integer.length() > 3) {
            StringBuilder tmp = new StringBuilder(integer);
            Integer i = integer.length() - 3;
            while (i > 0) {
                tmp.insert(i, ".");
                i = i - 3;
            }
            integer = tmp.toString();
        }
        return integer + decimal;


    }
}