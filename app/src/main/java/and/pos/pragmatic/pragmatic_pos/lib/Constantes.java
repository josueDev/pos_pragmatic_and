package and.pos.pragmatic.pragmatic_pos.lib;

/**
 * Clase que contiene los códigos usados en "I Wish" para
 * mantener la integridad en las interacciones entre actividades
 * y fragmentos
 */
public class Constantes {
    /**
     * Transición Home -> Detalle
     */
    public static final int CODIGO_DETALLE = 100;

    /**
     * Transición Detalle -> Actualización
     */
    public static final int CODIGO_ACTUALIZACION = 101;

    /**
     * Puerto que utilizas para la conexión.
     * Dejalo en blanco si no has configurado esta carácteristica.
     */
    private static final String PUERTO_HOST = "63343";
    private static final String PUERTO_HOST_GATEWAY = "63343";

    /**
     * Dirección IP de genymotion o AVD
     */
    private static final String IP = "http://10.0.3.2:";
    private static final String IPGateway = "https://test.openknowledge.mx:";

    /**
     * URLs del Web Service
     */
    public static final String GET = IP + PUERTO_HOST + "/I%20Wish/obtener_metas.php";
    public static final String GET_BY_ID = IP + PUERTO_HOST + "/I%20Wish/obtener_meta_por_id.php";
    public static final String UPDATE = IP + PUERTO_HOST + "/I%20Wish/actualizar_meta.php";
    public static final String DELETE = IP + PUERTO_HOST + "/I%20Wish/borrar_meta.php";
    public static final String INSERT = IP + PUERTO_HOST + "/I%20Wish/insertar_meta.php";
    public static final String POS = "https://schoolcalendar.gdt.com.mx/?idusuario=";
    public static final String getTicket = IPGateway + PUERTO_HOST_GATEWAY + "/gateway";
    public static final String POSPROD = "https://gateway-pms.test.openknowledge.mx:443/gateway";

    //get ticket
    //https://test.openknowledge.mx:8443/
    //TransactionName 046
    //WebTerminalConfigTicket 027
    //TerminalID 105
    //put TransactionName , WebTerminalConfigTicket
    //put TerminalID , terminalid

    /**
     * Clave para el valor extra que representa al identificador de una meta
     */
    public static final String EXTRA_ID = "IDEXTRA";

}